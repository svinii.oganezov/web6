function upPr() {
  let s = document.getElementsByName("prodType");
  let kolvo  = document.getElementById("kolvo").value;
  let select = s[0];
  let price = 0;
  let prices = getPrices();
  let priceIndex = parseInt(select.value) - 1;
  if (priceIndex >= 0) {
    price = prices.prodTypes[priceIndex];
  }

  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = (select.value == "1" ? "block" : "none");
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function (radio) {
    if (radio.checked) {
      let optionPrice = prices.prodOptions[radio.value];
      if (optionPrice !== undefined && select.value == "1") {
        price += optionPrice;
      }
    }
  });


  let checkDiv = document.getElementById("checkboxes");
  checkDiv.style.display = (select.value == "2" ? "block" : "none");
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function (checkbox) {
    if (checkbox.checked) {
      let propPrice = prices.prodProperties[checkbox.name];
      if (propPrice !== undefined && select.value == "2") {
        price += propPrice;
      }
    }
  });


  let prodPrice = document.getElementById("prodPrice");
  prodPrice.innerHTML = price * kolvo + " p";
}

function getPrices() {
  return {
    prodTypes: [800, 1200, 900],
    prodOptions: {
      pink: 200,
      black: 100,
      yellow: 0,
    },
    prodProperties: {
      prop1: 300,
      prop2: 100,
    }
  }
}
window.addEventListener("DOMContentLoaded", function (event) {
  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = "none";
  let number = document.getElementById("kolvo");
  number.addEventListener("change", function (event) {
    upPr();
  });
  let s = document.getElementsByName("prodType");
  let select = s[0];
  select.addEventListener("change", function (event) {  
    upPr();
  });
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function (radio) {
    radio.addEventListener("change", function (event) {
      upPr();
    });
  });
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function (checkbox) {
    checkbox.addEventListener("change", function (event) {
      upPr();
    });
  });

  upPr();
});

$(document).ready(function () {
    $("#myslidergg").slick({
        dots: true,
        infinite: true,
        arrows: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [{
            breakpoint: 600,
            settings: {
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }]
    });
});